# Moonpig Technical Test

https://github.com/markkevans/mp-tech-test

## Moonpig Frontend Technical Challenge
First of all I want to be clear that my experience as an engineer lies in integration and backend systems so prior
to starting this exercise I've never worked with React. This code was produced with basic knowledge of React and its
toolchain. It was also performed without TDD which ironically goes against everything I tell my developers unless
they are working on a spike and for me this is a big spike.

I set myself a time limit of two hours to perform this task and while it resolves navigation and fundamental
structure it does not attempt to resolve form. Those two hours include learning React from scratch...

Doing this again I would be looking to break down the requirements beyond the two statements and wireframes and
codify this as tests to verify the high level site needs and low level functional needs. I'd also look to use a
richer and more configurable query tool like GraphQL for handling request data.

## Design

### Product Details Class (ProductDetails)

The detailed card information from the product api should be wrapped behind a defined interface.
For the current requirements this only needs accessors for the title and image.

### Product Search Results Class (ProductSummary)

The summary information of products from the products api should be wrapped behind a defined
interface. For the current requirements this only needs accessors for the title, image and 
product number.

### Card Listings Component

The "card listings" view acts as a container for a series of products as thumbnails and a
mechanism for navigating to a detailed view of a selected product.

### Product Summary Component 

Individual products within the card listings view showing a thumbnail of each product and 
binding any accessibility information that might accompany it.

### Card Details Component

Detailed individual view of a single product - from the given requirements this is simply 
the full product image, a title and a button to initiate purchase. An option to navigate back 
to the listings view is necessary.

### Application Component

The application component holds the state values for the search result and a single card detail 
(if one is selected). This top level view also provides event wiring for navigation.

Loading of a single card detail only occurs once a selection is made to reduce any unnecessary 
traffic.

Further optimisation could be performed to cache a limited number of card details.

## Dependencies

Utilising React to provide a reliable foundation follows the latest trend on reactive web 
design, moving away from custom HTML and empowering HTML from javascript.

For simplicity the axios node module was used to abstract XMLHttpRequest, provide a promise 
mechanism and reduce the potential for XSRF.

No styling dependencies such as bootstrap css have been included but could easily be used 
to provide a significantly richer presentation.