import React, {Component} from 'react';
import axios from 'axios';
import './App.css';
// Style:
// bind bootstrap css or utilise material design
// function and structure are hard to separate
// but function and form should not be coupled
// form needs to be consistent and accessible - while not entirely relevant to a commercial website the GDS design
// patterns provide solid guidance to maintain accessibility without overly restricting artistic freedom
// https://paper.dropbox.com/doc/GOV.UK-Design-Patterns-Wiki-hkou7bpzKya8Dadv24V1z

// for simplicity this is reusing the search data rather than requesting the full details - no point while CORS issue
// is unresolved
// in the interests of keeping this as a true single page application the detail view has a "close" option added to
// permit easy navigation back to the search results
const CardDetail = (props) => {
  return (
        <div>
          <img src={props.item.ProductImage.Link.Href} alt={props.item.Title}/>
          <p>{props.item.Title}</p>
          <button>Buy Me</button>
          <button onClick={() => props.deselectItem()}>Close</button>
        </div>
  );
};

const Card = (props) => {
  return (
        <div onClick={() => props.selectItem(props)}>
          <img src={props.ProductImage.Link.Href} alt={props.Title}/>
        </div>
  );
};

// requires suitable grid flow rendering and, ideally, paging to focus and organise the items on the screen
// a grid works well on a desktop but something like a carousel would be better for small mobile devices
const CardList = (props) => {
  return (
        <div>
          {props.cards.map(card => <Card key={card.ProductId} {...card} selectItem={props.selectItem}/>)}
        </div>
  );
};

class App extends Component {
  constructor(props) {
    super(props);
    // by default CORS is blocking access - requires a suitable proxy configuration
    // axios is a quick (and dirty) solution to abstracting http requests that in retrospect has caused more problems
    // than it solves
    axios.get(`https://search.moonpig.com/api/products?size=12&searchFacets=occasion_level_3:occasion%3Ewell%20done%3Enew%20job`, {})
          .then(resp => {
            // for simplicity just adding the individual items to the state array of items
            // ideally the items should be wrapped with a consistent interface to provide consistent mutators
            // backend data can then be rewritten without (necessarily) rewriting the site code
            // using a class based approach to handle the data also allows the code to sanitise input and reduce any
            // potential for foul-play from manipulated data

            // in its current form of data the resp data could be concatenated to the cards state array but this
            // would need rewriting in its entirety to handle a class based approach to the card data (search or details)
            resp.data.map((item) => this.addNewCard(item));
          })
          .catch(error => {
            //respond gracefully to error
          });
  };

  state = {
    selectedItem: null,
    cards: [
      // {
      //   "Price": {
      //     "Value": 3.29,
      //     "Currency": "£"
      //   },
      //   "SoldOut": 0,
      //   "Title": "The Nightmare Before Christmas Personalised Good Luck Card",
      //   "ProductCategory": {
      //     "ProductCategoryId": 1,
      //     "Name": "greeting cards"
      //   },
      //   "PhotoUploadCount": 0,
      //   "CardShopId": 1,
      //   "DirectSmile": false,
      //   "DefaultSizeId": 1,
      //   "ProductId": 77879,
      //   "MoonpigProductNo": "nbc006",
      //   "TradingFaces": 0,
      //   "IsLandscape": 0,
      //   "ShortDescription": null,
      //   "IsCustomisable": 1,
      //   "IsMultipack": 0,
      //   "SeoPath": "the-nightmare-before-christmas-personalised-good-luck-card",
      //   "ProductCategoryGroupSeoPath": "personalised-cards/",
      //   "ProductLink": {
      //     "Href": "https://api-rest.moonpig.com/v2/products/nbc006/1",
      //     "Method": "GET",
      //     "Rel": "self",
      //     "Title": "77879"
      //   },
      //   "ProductImage": {
      //     "Link": {
      //       "Href": "https://dosrgfkou9o2m.cloudfront.net/api/images/CardShop/1/product/nbc006",
      //       "Method": "GET",
      //       "Rel": "self",
      //       "Title": "77879"
      //     },
      //     "MimeType": "image/jpeg"
      //   },
      //   "Reviews": {
      //     "MinReviewData": 0,
      //     "MaxReviewData": 0,
      //     "AverageStarReviewRating": 5.0,
      //     "ReviewCount": 4
      //   }
      // },
      // {
      //   "Price": {
      //     "Value": 8.0,
      //     "Currency": "£"
      //   },
      //   "SoldOut": 0,
      //   "Title": "Pink Name Coffee Personalised Mug",
      //   "ProductCategory": {
      //     "ProductCategoryId": 11,
      //     "Name": "mugs"
      //   },
      //   "PhotoUploadCount": 0,
      //   "CardShopId": 1,
      //   "DirectSmile": false,
      //   "DefaultSizeId": 27,
      //   "ProductId": 76402,
      //   "MoonpigProductNo": "fc042-mu",
      //   "TradingFaces": 0,
      //   "IsLandscape": 1,
      //   "ShortDescription": null,
      //   "IsCustomisable": 1,
      //   "IsMultipack": 0,
      //   "SeoPath": "pink-name-coffee-personalised-mug",
      //   "ProductCategoryGroupSeoPath": "gifts/",
      //   "ProductLink": {
      //     "Href": "https://api-rest.moonpig.com/v2/products/fc042-mu/27",
      //     "Method": "GET",
      //     "Rel": "self",
      //     "Title": "76402"
      //   },
      //   "ProductImage": {
      //     "Link": {
      //       "Href": "https://dosrgfkou9o2m.cloudfront.net/api/images/CardShop/1/product/fc042-mu",
      //       "Method": "GET",
      //       "Rel": "self",
      //       "Title": "76402"
      //     },
      //     "MimeType": "image/jpeg"
      //   },
      //   "Reviews": {
      //     "MinReviewData": 0,
      //     "MaxReviewData": 0,
      //     "AverageStarReviewRating": 4.8929,
      //     "ReviewCount": 28
      //   }
      // }
    ]
  };
  addNewCard = (cardInfo) => {
    this.setState(prevState => ({
      cards: prevState.cards.concat(cardInfo)
    }));
  };
  selectItem = (props) => {
    this.setState({
      // fetch item from moonpig catalogue
      // currently just re-uses single search item
      selectedItem: props
    });
  };
  deselectItem = () => {
    this.setState({
      selectedItem: null
    });
  };

  render() {
    const {selectedItem, cards} = this.state;
    if (selectedItem === null) {
      return (
            <div className="App">
              <CardList cards={cards} selectItem={this.selectItem}/>
            </div>
      );
    } else {
      return (
            <div className="App">
              <CardDetail item={selectedItem} deselectItem={this.deselectItem}/>
            </div>
      );
    }
  }
}

export default App;
